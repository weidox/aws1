'use strict';
import { SendMailOptions, createTransport } from 'nodemailer';
import email from 'email-templates';
import aws from 'aws-sdk';

// module.exports.hello = async event => {
export async function hello(event: any) {

  let transporter = createTransport({
    SES: new aws.SES()//({ apiVersion: '2010-12-01' })
  });
  
  //aws.config.loadFromPath('./config.json');

  const sender = "Sender Name <weidox@gmail.com>";
  const recipient = "weidox@gmail.com";
  const configuration_set = "ConfigSet";
  const subject = "SES2 test";
  const body_text = "SES2 test\r\TXT";
  const body_html = `<html>
  <head></head>
  <body>
    <h1>SES2 test</h1>
    <p>HTML</p>
  </body>
  </html>
  `
  const charset = "UTF-8";

  let params: SendMailOptions = {
    from: sender,
    to: recipient,
    subject: subject,
    text: body_text,
    html: body_html
  };

  let msg;
  await transporter.sendMail(params)
    .then(data => {
      msg = "OK " + data;
      console.log(msg)
    })
    .catch(error => {
      msg = "ERR " + error;
      console.log(msg)
    });
  ;
  

  /*

  

  console.log('creating new SES');
  let ses = new aws.SES();

  let params = {
    Source: sender, 
    ReturnPath: sender,
    Destination: { 
      ToAddresses: [
        recipient 
      ],
    },
    Message: {
      Subject: {
        Data: subject,
        Charset: charset
      },
      Body: {
        Text: {
          Data: body_text,
          Charset: charset 
        },
        Html: {
          Data: body_html,
          Charset: charset
        }
      }
    }//,
    //ConfigurationSetName: configuration_set
  };
  console.log('going to send');
  let msg;
  let sendEmail = ses.sendEmail(params).promise();
  console.log('got promise');
  console.log(sendEmail);
  await sendEmail
    .then(data => {
      msg = 'OK ' + data;
    console.log(data);
    console.log(msg);
    })
    .catch(error => {
      msg = 'ERR ' + error;
    console.log(error);
    console.log(msg);
    });
  console.log("final" + msg);
  */

  /*  
  let ses = require('node-ses');
  client = ses.createClient();
  
  // Give SES the details and let it construct the message for you.
  client.sendEmail({
    to: 'weidox@gmail.com'
  , from: 'weidox@gmail.com'
  , subject: 'ses test 2'
  , message: 'your <b>message</b> goes here'
  , altText: 'plain text'
  }, function (err, data, res) {
  console.log(err);
  console.log(data);
  console.log(res);
  });
  */

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'GO!' + msg,
        input: event,
      },
      null,
      2
    ),
  };

    // Use this code if you don't use the http event with the LAMBDA-PROXY integration
    // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.hello();